﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using simple_API.DTOs;

namespace simple_API.Model
{
    [Route("api/v1/climbing-shoe")]
    [ApiController]
    public class ClimbingShoesController : ControllerBase
    {
        private readonly SimpleApiDbContext _context;
        private readonly IMapper _mapper;

        public ClimbingShoesController(SimpleApiDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/climbing-shoe
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClimbingShoeDto>>> GetClimbingShoes()
        {
            var shoes = await _context.ClimbingShoes.ToListAsync();

            return shoes.Select(s => _mapper.Map<ClimbingShoe, ClimbingShoeDto>(s)).ToList();
        }

        // GET: api/v1/climbing-shoe/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClimbingShoeDto>> GetClimbingShoe(int id)
        {
            var climbingShoe = await _context.ClimbingShoes.FindAsync(id);

            if (climbingShoe == null)
            {
                return NotFound();
            }

            return _mapper.Map<ClimbingShoe, ClimbingShoeDto>(climbingShoe);
        }

        // PUT: api/v1/climbing-shoe/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClimbingShoe(int id, ClimbingShoe climbingShoe)
        {
            if (id != climbingShoe.Id)
            {
                return BadRequest();
            }

            _context.Entry(climbingShoe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClimbingShoeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/climbing-shoe
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ClimbingShoe>> PostClimbingShoe(ClimbingShoe climbingShoe)
        {
            _context.ClimbingShoes.Add(climbingShoe);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClimbingShoe", new { id = climbingShoe.Id }, climbingShoe);
        }

        // DELETE: api/v1/climbing-shoe/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClimbingShoeDto>> DeleteClimbingShoe(int id)
        {
            var climbingShoe = await _context.ClimbingShoes.FindAsync(id);
            if (climbingShoe == null)
            {
                return NotFound();
            }

            _context.ClimbingShoes.Remove(climbingShoe);
            await _context.SaveChangesAsync();

            return _mapper.Map<ClimbingShoe, ClimbingShoeDto>(climbingShoe);
        }

        private bool ClimbingShoeExists(int id)
        {
            return _context.ClimbingShoes.Any(e => e.Id == id);
        }
    }
}
