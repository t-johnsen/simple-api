﻿using AutoMapper;
using simple_API.DTOs;
using simple_API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simple_API.Profiles
{
    public class ClimbingShoeProfile : Profile
    {
        public ClimbingShoeProfile()
        {
            CreateMap<ClimbingShoe, ClimbingShoeDto>();

        }
    }
}

