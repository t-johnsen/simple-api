﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simple_API.Model
{
    public class SimpleApiDbContext : DbContext
    {
        public DbSet<ClimbingShoe> ClimbingShoes { get; set; }

        public SimpleApiDbContext(DbContextOptions options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
