﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace simple_API.Model
{
    public class ClimbingShoe
    {

        public int Id { get; set; }

        [Column(TypeName = "varchar(50)"), Required]
        public string Type { get; set; }
        public int Size { get; set; }

        [Column(TypeName = "varchar(50)"), Required]
        public string RubberType { get; set; }

        [Column(TypeName = "varchar(50)"), Required]
        public string Brand { get; set; }

        public int Painlevel { get; set; }
    }
}
