﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simple_API.DTOs
{
    public class ClimbingShoeDto
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public string RubberType { get; set; }
        public string Brand { get; set; }
        public int Painlevel { get; set; }
    }
}
