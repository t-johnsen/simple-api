﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace simple_API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClimbingShoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "varchar(50)", nullable: false),
                    Size = table.Column<int>(nullable: false),
                    RubberType = table.Column<string>(type: "varchar(50)", nullable: false),
                    Brand = table.Column<string>(type: "varchar(50)", nullable: false),
                    Painlevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClimbingShoes", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ClimbingShoes",
                columns: new[] { "Id", "Type", "Size", "RubberType", "Brand", "Painlevel" },
                values: new object[,]
                {
                    {1, "Aggresive", 40, "Vibram XS grip", "Scarpa", 100 },
                    {2, "Flat", 36, "Vibram XS grip 2", "Scarpa", 87 },
                    {3, "Flat", 41, "Vibram Edge", "Scarpa", 84 },
                    {4, "Intermediate", 40, "Vibram XS grip", "Scarpa", 100 },

                }
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClimbingShoes");
        }
    }
}
