# Simple API made to test out Docker functionality
An ASP.NET Core API with Docker functionality. 
## Getting started and usage
1. Clone to a local directory.
2. Open solution in Visual Studio (VS).
3. (Optional. Works with default values) Insert your credentials in the ```docker-compose.yml``` file under the *ConnectionStrings* propertie and then *Default*.
4. Choose between step 5 and 6 depending on how you will run it.
5. To run the application locally, create/update database and run program:
    - Insert your credentials in the ```appsettings.json``` file under the *ConnectionStrings* propertie and then *Default*. Under the *ConfigureServices* in ```Startup.cs``` the database context is added as an SQL server.
    - ```Data Source```-> database
    - ```Initial Catalog``` -> name on database (will make database for you if it does not exist).
    - Update database with .NET Core CLI -> ```dotnet ef database update```
    - Or update database with NuGet PowerShell -> ```update-database```
    - Run program in VS (hotkey: F5). It will open a new browser window.
6. Run the application using Docker Compose:
    - docker-compose up --build
    - Open browser and go to localhost/api/v1/climbing-shoes
    - (See endpoints for other resources)

For more information about migrations see documentation [Entity Framework Core].
If you choose to work with Docker, I recommend using Visual Studio Code instead of Visual Studio.

## Prerequisites
* Docker CLI tools
* .NET Framework
* Visual Studio 2017/19 OR Visual Studio Code

### Packages:
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.Sqlite
* Microsoft.EntityFrameworkCore.Tools
* AutoMapper.Extensions.Microsoft.DependencyInjection
* Packages for Swagger
* (Microsoft.VisualStudio.Web.CodeGeneration.Design - not needed to run api, but is used for generating basic Controllers for Models)


[//]: #
[Entity Framework Core]: <https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli>
[Swagger]: <https://swagger.io/>
